const usersApp = require('../../src/js/userApp')

describe('usersApp', () => {
  describe('find()', () => {
    test('give first user if parameter is 0', () => {
      const user = {
        id: 0,
        firstname: 'John',
        lastname: 'Doe'
      }
      usersApp.users = []
      usersApp.users.push(user)
      expect(usersApp.find(0)).toEqual(user)
    })

    test('give null if user is not exists', () => {
      expect(usersApp.find(100)).toBeNull()
    })
    test('give Wrong id type Error id parameter is not a integer', () => {
      expect(usersApp.find('a')).toEqual(Error('Wrong id type'))
    })

    test('give Missing id Error if id parameter is missing', () => {
      expect(usersApp.find()).toEqual(Error('Missing id'))
    })
  })

  describe('findAll()', () => {
    const users = [{
      id: 0,
      firstname: 'John',
      lastname: 'Doe'
    },
    {
      id: 1,
      firstname: 'Jane',
      lastname: 'Doe'
    }]
    usersApp.users = users
    expect(usersApp.findAll()).toEqual(users)
  })
})
