const PeopleObjectApp = require('../../src/js/peopleObjectApp.js')

const app = new PeopleObjectApp()

describe('peopleObjectApp ', () => {
  beforeEach(() => {
    app.people = []
    app.add({
      firstname: 'John',
      lastname: 'Doe'
    })
  })

  test('list person should equal { firstname: "John", lastname: "Doe" }', () => {
    const actual = app.list()
    const expected = [{ firstname: 'John', lastname: 'Doe' }]
    expect(actual).toEqual(expected)
  })

  test('add person list person should equal', () => {
    const user = {
      firstname: 'Jane',
      lastname: 'Doe'
    }
    app.add(user)
    const actual = app.people[1]
    const expected = user
    expect(actual).toEqual(expected)
  })

  test('Edit person', () => {
    const user = {
      firstname: 'Jane',
      lastname: 'Doe'
    }
    app.edit(0, user)
    expect(app.people[0]).toEqual(user)
  })

  test('Delete person', () => {
    app.delete(0)
    // const actual = app.people
    // const expected = []
    expect(app.people).toEqual([])
  })

  test('Number of people', () => {
    // const actual = app.numberOfPeople()
    // const expected = 1
    expect(app.numberOfPeople()).toEqual(1)
  })
})
