const uppercase = require('../../src/js/callback')

describe('uppercase ', () => {
  test('\'test\' should be \'TEST\'', (done) => {
    uppercase((str) => {
      expect(str).toBe('TEST')
      done()
    }, 'test')
  })

  test('default paramter value \'sample\' should be \'SAMPLE\'', (done) => {
    uppercase((str) => {
      expect(str).toBe('SAMPLE')
      done()
    })
  })

  test('give \'Not a string\' Error if the first parameter is not a string', () => {
    const actual = uppercase(function () { }, 100)
    const expected = Error('Not a string')
    expect(actual).toEqual(expected)
  })
})
