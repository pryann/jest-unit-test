const uppercase = require('../../src/js/promise')

describe('uppercase ', () => {
  // test('\'test\' should to \'TEST\'', () => {
  //   uppercase('test')
  //     .then(str => {
  //       expect(str).toBe('TEST')
  //     })
  // })

  // test('default paramter value \'sample\' it should to \'SAMPLE\'', () => {
  //   uppercase()
  //     .then(str => {
  //       expect(str).toBe('SAMPLE')
  //     })
  // })

  // test('give \'Type Error\' if parameter is not a string', () => {
  //   uppercase(100)
  //     .catch(e => {
  //       expect(e).toEqual(Error('Type error'))
  //     })
  // })

  test('\'test\' should to \'TEST\'', async () => {
    // 1. verzió
    // const actual = await uppercase('test')
    // const epxected = 'TEST'
    // expect(actual).toBe(epxected)
    // 2. verzió
    await expect(uppercase('test')).resolves.toBe('TEST')
  })

  test('default paramter value \'sample\' it should to \'SAMPLE\'', async () => {
  //   // const actual = await await uppercase()
  //   // const epxected = 'SAMPLE'
  //   // expect(actual).toBe(epxected)
    await expect(uppercase()).resolves.toBe('SAMPLE')
  })

  test('give \'Type Error\' if parameter is not a string', async () => {
  //   // const actual = await uppercase(100)
  //   // const epxected = Error('Type error')
  //   // uppercase(actual).toEqual(epxected)
    await expect(uppercase(100)).rejects.toEqual(Error('Type error'))
  })
})
