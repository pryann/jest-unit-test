const forEach = require('../../src/js/forEach')

describe('forEach ', () => {
  test('test1', () => {
    const mockCallback = jest.fn(x => x * 2)

    forEach([1, 2, 3], mockCallback)
    expect(mockCallback.mock.calls.length).toBe(3)
    expect(mockCallback.mock.calls[0][0]).toBe(1)
    expect(mockCallback.mock.results[0].value).toBe(2)
  })
})
