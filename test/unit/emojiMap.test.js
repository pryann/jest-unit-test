const { searchMovies, emojiMap } = require('../../src/js/emojiMap')

describe('searchMovies ', () => {
//   test('returns the movies that match the query', () => {
//     expect(searchMovies('F')).toEqual([
//       { title: 'Frozen', emoji: '⏰' },
//       { title: 'Finding Nemo', emoji: '⌛' },
//       { title: 'Kung Fu Panda', emoji: '♈' }
//     ])
//   })

  test('returns the movies that match the query', () => {
    expect.addSnapshotSerializer({
      test: (val) => val.title && val.emoji,
      print: (val) => `${val.emoji} ${val.title}`
    })
    expect(searchMovies('F')).toMatchSnapshot()
  })
})
