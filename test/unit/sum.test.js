const { sum, errorMessages } = require('../../src/js/sum.js')
describe('sum ', () => {
  test('1 + 2 should be 3', () => {
    const actual = sum(1, 2)
    const expected = 3
    expect(actual).toBe(expected)
  })

  test('0.1 + 0.2 should be close to 0.3', () => {
    const actual = sum(0.1, 0.2)
    const expected = 0.3
    expect(actual).toBeCloseTo(expected)
  })

  test('with default values should be 0', () => {
    const actual = sum()
    const expected = 0
    expect(actual).toBe(expected)
  })

  test('give \'paramerter Error\' if parameters are not numbers', () => {
    const actual = sum('a', 2)
    const expected = Error(errorMessages.paramerter)
    expect(actual).toEqual(expected)
  })

  test('give \'Not a number Error\' if parameters are NaN', () => {
    const actual = sum(NaN, 2)
    const expected = Error(errorMessages.notANumber)
    expect(actual).toEqual(expected)
  })
})
