const {
  sumVersionTwo,
  errorMessages
} = require('../../src/js/sumVersionTwo')

describe('sumVersionTwo ', () => {
  test('1 + 2 should be 3', () => {
    const actual = sumVersionTwo(1, 2)
    const expected = 3
    expect(actual).toBe(expected)
  })

  test('0.1 + 0.2 should be close to 0.3', () => {
    const actual = sumVersionTwo(0.1, 0.2)
    const expected = 0.3
    expect(actual).toBeCloseTo(expected)
  })

  test('with default values should be 0', () => {
    const actual = sumVersionTwo()
    const expected = 0
    expect(actual).toBe(expected)
  })

  test('give \'paramerter Error\' if parameters are not numbers', () => {
    const actual = () => sumVersionTwo('10', 2)
    const expected = errorMessages.paramerter
    expect(actual).toThrowError(expected)
  })

  test('give \'Not a number Error\' if parameters are NaN', () => {
    const actual = () => sumVersionTwo(NaN, 2)
    const expected = errorMessages.notANumber
    expect(actual).toThrowError(expected)
  })
})
