const usersApp = {
  users: [],
  find (id) {
    if (id === undefined) {
      return Error('Missing id')
    }
    if (!Number.isInteger(id)) {
      return Error('Wrong id type')
    }
    return this.users.find(users => users.id === id) || null
  },
  findAll () {
    return this.users
  }
}

module.exports = usersApp
