const uppercase = (str = 'sample') => {
  return new Promise((resolve, reject) => {
    if (typeof str !== 'string') {
      reject(Error('Type error'))
    }
    resolve(str.toUpperCase())
  })
}

module.exports = uppercase
