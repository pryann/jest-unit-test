const emojiMap = [
  { title: 'Frozen', emoji: '⏰' },
  { title: 'Frozen2', emoji: '⏰' },
  { title: 'Finding Nemo', emoji: '⌛' },
  { title: 'Kung Fu Panda', emoji: '♈' },
  { title: 'Harry Potter', emoji: '⚡' },
  { title: 'Hunger Games', emoji: '🖐' },
  { title: 'The Lion King', emoji: '🔪' },
  { title: 'ET', emoji: '🛌' }
]

function searchMovies (query) {
  return emojiMap.filter((movie) => {
    return movie.title.includes(query)
  })
}

module.exports = { emojiMap, searchMovies }
