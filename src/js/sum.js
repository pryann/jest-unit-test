const errorMessages = {
  paramerter: 'Parameter error',
  notANumber: 'Not a number error'
}

const sum = (a = 0, b = 0) => {
  if (typeof a !== 'number' || typeof b !== 'number') {
    return Error(errorMessages.paramerter)
  }

  if (Number.isNaN(a) || Number.isNaN(b)) {
    return Error(errorMessages.notANumber)
  }
  return a + b
}

module.exports = {
  sum, errorMessages
}
