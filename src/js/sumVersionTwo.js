const {
  errorMessages
} = require('./sum')

const sumVersionTwo = (a = 0, b = 0) => {
  if (typeof a !== 'number' || typeof b !== 'number') {
    throw new Error(errorMessages.paramerter)
  }

  if (Number.isNaN(a) || Number.isNaN(b)) {
    throw new Error(errorMessages.notANumber)
  }
  return a + b
}

module.exports = {
  sumVersionTwo, errorMessages
}
