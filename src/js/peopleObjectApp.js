class PeopleObjectApp {
  // person = {firstName: 'John', lastName: 'Doe'} required
  constructor () {
    this.people = []
  }

  list () {
    return this.people
  }

  add (person) {
    this.people.push(person)
    return this
  }

  edit (index, person) {
    this.people[index] = person
    return this
  }

  delete (index) {
    this.people.splice(index, 1)
    return this
  }

  numberOfPeople () {
    return this.people.length
  }
}

module.exports = PeopleObjectApp
