const uppercase = (callback, str = 'sample') => {
  if (typeof str !== 'string') {
    return Error('Not a string')
  } callback(str.toUpperCase())
}

module.exports = uppercase
